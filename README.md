**Flink - Android Challenge**

*Luis David Ruiz Wence*

*luisdavidruizwence@hotmail.com*

Solución al reto de programación de Android. La aplicación está diseñada como un buscador de tienda en linea muy básico: un EditText para buscar por el nombre del personaje, un ImageView con icono de filtro a un lado del EditText que al darle clic despliega un menú flotante con opciones, y un RecyclerView que se muestra debajo del EditText con los resultados.

Al dar click en el EditText se muestra otro RecyclerView con un TextView encima en caso de que no haya búsquedas recientes, de lo contrario el RecyclerView muestra el texto de las búsquedas mas recientes que fueron exitosas.

---

## Bibliotecas usadas

**Droppy**: https://github.com/shehabic/Droppy

ViewGroup flotante sobre otros elementos que se oculta al dar click fuera de el. Usado en el fitro de búsqueda.

**ExpandIcon**: https://github.com/zagum/Android-ExpandIcon

Elemento visual que ayuda al usuario a identificar una accción de expandir/contraer un elemento. Usado en el fitro de búsqueda.

**Glide**: https://github.com/bumptech/glide

Usado para administrar la carga y caché de imágenes

**Gson**: https://github.com/google/gson

Usado para pasar las respuestras del API a objetos y como utilidad en otras tareas

**Retrofit2**: https://github.com/square/retrofit

Usado para el consumo del API

**OKHttp3 Logging Interceptor**: https://github.com/square/okhttp/tree/master/okhttp-logging-interceptor

Usado en conjunto con Retrofit para observar en Logcat los detalles de las peticiones y respuestas del API

**Kotlinx.corutines**: https://github.com/Kotlin/kotlinx.coroutines

Usadas para el manejo de tareas que requieren ser asíncronas, como el consumo del API y el uso de Room

**ViewModel**: https://developer.android.com/jetpack/androidx/releases/lifecycle

androidx.lifecycle:lifecycle-viewmodel-ktx:2.2.0

Usado para poder implementar la arquitectura MVVM

**Koin**: https://github.com/InsertKoinIO/koin

Ussado para la inyección de dependencias

**Room**: https://developer.android.com/topic/libraries/architecture/room

androidx.room:room-runtime:2.2.5, androidx.room:room-ktx:2.2.5

Usado para guardar las búsquedas recientes

package com.miflink.rickandmorty.utils.ws

import com.google.gson.annotations.SerializedName

open class ErrorResponse (@SerializedName("error") val error: String?)
package com.miflink.rickandmorty.utils.ws

import java.net.ConnectException
import java.net.SocketTimeoutException
import java.net.UnknownHostException
import java.security.cert.CertPathValidatorException

fun handleThrowable(t: Throwable): String = when(t){
    is SocketTimeoutException, is ConnectException, is UnknownHostException,
    is CertPathValidatorException -> "The server cannot be reached, try again later"
    else -> "Unknown error"
}
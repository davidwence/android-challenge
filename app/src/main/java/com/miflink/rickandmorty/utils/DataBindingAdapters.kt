package com.miflink.rickandmorty.utils

import android.view.View
import androidx.annotation.DrawableRes
import androidx.databinding.BindingAdapter

@Suppress("unused")
object DataBindingAdapters {

    @JvmStatic
    @BindingAdapter("backgroundResource")
    fun setBackgroundResource(view: View, @DrawableRes id: Int?) {
        if(id != null)
            view.setBackgroundResource(id)
        else
            view.setBackgroundResource(0)
    }

}
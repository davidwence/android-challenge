package com.miflink.rickandmorty.utils;

import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import org.jetbrains.annotations.NotNull;

@SuppressWarnings("unused")
public abstract class EndlessRecyclerViewScrollListener extends RecyclerView.OnScrollListener {
    // The maximum pages to limit the "endless" loading resulting in memory overflow
    private int maximumPages = 50;
    private int maximumElements = 1000;
    // The minimum amount of items to have below your current scroll position
    // before loading more.
    private int visibleThreshold = 10;
    // The current offset index of data you have loaded
    private int currentPage = 0;
    // The total number of items in the dataset after the last load
    private int previousTotalItemCount = 0;
    // True if we are still waiting for the last set of data to load.
    private boolean loading = true, blockUntilMoreLoaded = false, limitReached = false;
    // Sets the starting page index
    private int startingPageIndex = 0;

    private RecyclerView.LayoutManager mLayoutManager;

    public EndlessRecyclerViewScrollListener(RecyclerView.LayoutManager layoutManager, int pageSize){
        this.mLayoutManager = layoutManager;
        if(layoutManager instanceof GridLayoutManager){
            configureThresholdAndMaxPages(pageSize);
            visibleThreshold = visibleThreshold * ((GridLayoutManager)layoutManager).getSpanCount();
        } else if(layoutManager instanceof LinearLayoutManager) {
            configureThresholdAndMaxPages(pageSize);
        } else if(layoutManager instanceof StaggeredGridLayoutManager){
            configureThresholdAndMaxPages(pageSize);
            visibleThreshold = visibleThreshold * ((StaggeredGridLayoutManager)layoutManager).getSpanCount();
        }
    }

    protected EndlessRecyclerViewScrollListener(LinearLayoutManager layoutManager,
                                                int pageSize) {
        this.mLayoutManager = layoutManager;
        configureThresholdAndMaxPages(pageSize);
    }

    public EndlessRecyclerViewScrollListener(GridLayoutManager layoutManager,
                                             int pageSize) {
        this.mLayoutManager = layoutManager;
        configureThresholdAndMaxPages(pageSize);
        visibleThreshold = visibleThreshold * layoutManager.getSpanCount();
    }

    public EndlessRecyclerViewScrollListener(StaggeredGridLayoutManager layoutManager,
                                             int pageSize) {
        this.mLayoutManager = layoutManager;
        configureThresholdAndMaxPages(pageSize);
        visibleThreshold = visibleThreshold * layoutManager.getSpanCount();
    }

    private void configureThresholdAndMaxPages(int pageSize){
        if(pageSize >= 5) {
            this.visibleThreshold = pageSize / 2;
            if(pageSize * maximumPages > maximumElements)
                maximumPages = maximumElements/pageSize;
        }
    }

    private int getLastVisibleItem(int[] lastVisibleItemPositions) {
        int maxSize = 0;
        for (int i = 0; i < lastVisibleItemPositions.length; i++) {
            if (i == 0) {
                maxSize = lastVisibleItemPositions[i];
            } else if (lastVisibleItemPositions[i] > maxSize) {
                maxSize = lastVisibleItemPositions[i];
            }
        }
        return maxSize;
    }

    public int getCurrentPage(){
        return currentPage;
    }

    public void setCurrentPage(int currentPage){
        this.currentPage = currentPage;
    }

    // Since every change in total item count (adding) is validated as "load complete", we need to block
    // listening for changes manually if we add some status views in recyclerview (like loading or retry)
    public void blockUntilMoreLoaded(boolean block){
        blockUntilMoreLoaded = block;
    }

    // This happens many times a second during a scroll, so be wary of the code you place here.
    // We are given a few useful parameters to help us work out if we need to load some more data,
    // but first we check if we are waiting for the previous load to finish.
    @Override
    public void onScrolled(@NotNull RecyclerView view, int dx, int dy) {
        if(!blockUntilMoreLoaded && currentPage < maximumPages - 1) {
            int lastVisibleItemPosition = 0;
            int totalItemCount = mLayoutManager.getItemCount();

            if (mLayoutManager instanceof StaggeredGridLayoutManager) {
                int[] lastVisibleItemPositions = ((StaggeredGridLayoutManager) mLayoutManager)
                        .findLastVisibleItemPositions(null);
                // get maximum element within the list
                lastVisibleItemPosition = getLastVisibleItem(lastVisibleItemPositions);
            } else if (mLayoutManager instanceof GridLayoutManager) {
                lastVisibleItemPosition = ((GridLayoutManager) mLayoutManager)
                        .findLastVisibleItemPosition();
            } else if (mLayoutManager instanceof LinearLayoutManager) {
                lastVisibleItemPosition = ((LinearLayoutManager) mLayoutManager)
                        .findLastVisibleItemPosition();
            }

//        Log.d("EndlessRecyclerViewScr", "total: "+totalItemCount+", ultimoVisible: "+lastVisibleItemPosition);

            // If the total item count is zero and the previous isn't, assume the
            // list is invalidated and should be reset back to initial state
            if (totalItemCount < previousTotalItemCount) {
                this.currentPage = this.startingPageIndex;
                this.previousTotalItemCount = totalItemCount;
                if (totalItemCount == 0) {
                    this.loading = true;
                }
            }
            // If it’s still loading, we check to see if the dataset count has
            // changed, if so we conclude it has finished loading and update the current page
            // number and total item count.
            if (loading && (totalItemCount > previousTotalItemCount)) {
                loading = false;
                previousTotalItemCount = totalItemCount;
            }

            // If it isn’t currently loading, we check to see if we have breached
            // the visibleThreshold and need to reload more data.
            // If we do need to reload some more data, we execute onLoadMore to fetch the data.
            // threshold should reflect how many total columns there are too
            if (!loading && (lastVisibleItemPosition + visibleThreshold) > totalItemCount) {
                onLoadMore(++currentPage, totalItemCount, view);
                loading = true;
            }
        } else if(currentPage >= maximumPages - 1 && !limitReached) {
            limitReached = true;
            onLimitReached(currentPage, mLayoutManager.getItemCount(), view);
        }
    }

    // Call this method whenever performing new searches
    public void resetState() {
        this.currentPage = this.startingPageIndex;
        this.previousTotalItemCount = 0;
        this.loading = true;
        this.blockUntilMoreLoaded = false;
        this.limitReached = false;
    }

    // Defines the process for actually loading more data based on page
    public abstract void onLoadMore(int page, int totalItemsCount, @NotNull RecyclerView view);

    // User can implement this to have a callback
    public void onLimitReached(int page, int totalItemsCount, @NotNull RecyclerView view){

    }
}

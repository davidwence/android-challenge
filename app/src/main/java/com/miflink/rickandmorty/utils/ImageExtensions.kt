package com.miflink.rickandmorty.utils

import android.graphics.drawable.Drawable
import android.net.Uri
import android.text.TextUtils
import android.util.Base64
import android.util.Base64OutputStream
import android.util.Log
import android.view.View
import android.webkit.MimeTypeMap
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.Glide
import com.bumptech.glide.RequestManager
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.RequestOptions
import com.bumptech.glide.request.target.Target
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileInputStream

object ImageBindingAdapter{

    @JvmStatic
    @BindingAdapter(value = ["imageUrl", "loader", "fallback", "isCircular"], requireAll = false)
    fun loadImageWithGlide(imageView: ImageView, url: String?, loader: View?, fallback: View?,
                           circular: Boolean?) {
        if (url != null && url.isValid() && url != "null")
            try {
                loadImage(
                    GlideApp.with(imageView.context), url, imageView, loader, fallback, circular)
            } catch (e: Exception) {
                Log.e("ImagenUtil", e.message, e)
                if (fallback != null)
                    fallback.visibility = View.VISIBLE
                else
                    loader?.show()
            }
        else {
            Log.w("ImageBindingAdapter","url not valid")
            if (fallback != null)
                fallback.visibility = View.VISIBLE
            else
                loader?.show()
        }
    }

    private fun loadImage(manager: RequestManager,
                          url: String,
                          imagen: ImageView,
                          loader: View?,
                          errorFallback: View?,
                          isCircular: Boolean? = false) {
        try {
            loader?.show()
            if (!TextUtils.isEmpty(url)) {
                val request = manager
                    .load(Uri.parse(url))
                    .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                    .listener(object: RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean): Boolean {
                            errorFallback?.run {
                                show()
                                loader?.hide()
                            }
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean): Boolean {
                            errorFallback?.hide()
                            loader?.hide()
                            return false
                        }

                    })
                if(isCircular == true)
                    request.apply(RequestOptions.bitmapTransform(CircleCrop()))
                else
                    request.fitCenter()
                        .transition(
                            DrawableTransitionOptions.withCrossFade(350))
                        .dontTransform()
                request.into(imagen)
            }
        } catch (e: Exception) {
            Log.e("ImageBindingAdapter", e.message, e)
            errorFallback?.run {
                show()
                loader?.hide()
            }
        }
    }
}
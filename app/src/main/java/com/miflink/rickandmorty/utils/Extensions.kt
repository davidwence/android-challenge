package com.miflink.rickandmorty.utils

import android.app.Activity
import android.content.Context
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.annotation.LayoutRes
import com.google.android.material.snackbar.Snackbar

fun View.show(){
    visibility = View.VISIBLE
}

fun View.hide(){
    visibility = View.GONE
}

fun Activity.showSnackbar(message: String){
    if(message.isValid())
        Snackbar.make(findViewById<ViewGroup>(android.R.id.content).getChildAt(0),
            message,
            Snackbar.LENGTH_SHORT).show()
}

fun String?.isValid(): Boolean {
    if (this == null)
        return false
    return !TextUtils.isEmpty(this.trim { it <= ' ' })
}

fun EditText.extractData(): String { return text.toString().trim { it <= ' ' } }

fun Activity.hideKeyboard() {
    val view = findViewById<View>(android.R.id.content)
    if (view != null)
        (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .hideSoftInputFromWindow(findViewById<View>(android.R.id.content).windowToken, 0)
}

fun Activity.showKeyboard(){
    (getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager).
        toggleSoftInput(
            InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
}

fun View.disable(){
    isEnabled = false
    isClickable = false
    alpha = 0.5f
}

fun View.enable(){
    isEnabled = true
    isClickable = true
    alpha = 1f
}

fun ViewGroup.layoutInflater(): LayoutInflater = LayoutInflater.from(context)

fun ViewGroup.inflateView(@LayoutRes id: Int): View =
    layoutInflater().inflate(id,this, false)
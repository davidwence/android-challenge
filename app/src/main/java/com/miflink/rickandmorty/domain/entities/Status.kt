package com.miflink.rickandmorty.domain.entities

import androidx.annotation.DrawableRes
import com.miflink.rickandmorty.R
import java.util.*

@Suppress("unused")
enum class Status (val tag: String, val label: String, @DrawableRes val backgroundId: Int) {
    NA("", "Any", R.drawable.background_statusunkwonw),
    DEAD("dead", "Dead", R.drawable.background_statusdead),
    ALIVE("alive", "Alive", R.drawable.background_statusalive),
    UNKNOWN("unknown", "Unknown", R.drawable.background_statusunkwonw);

    override fun toString(): String {
        return label
    }

    companion object{
        const val TITLE = "Status"

        fun getFromTag(tag: String?): Status{
            if(tag != null)
                for(status in values())
                    if(status.tag == tag.toLowerCase(Locale.getDefault()))
                        return status
            return Status.NA
        }
    }
}
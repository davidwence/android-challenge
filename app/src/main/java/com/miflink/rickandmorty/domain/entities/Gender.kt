package com.miflink.rickandmorty.domain.entities

import java.util.*

@Suppress("unused")
enum class Gender (val tag: String, val label: String) {
    NA("","Any"),
    MALE("male","Male"),
    FEMALE("female","Female"),
    UNKWONW("unknown", "Unknown"),
    GENDERLESS("genderless", "Genderless");

    override fun toString(): String {
        return label
    }

    companion object{
        const val TITLE = "Gender"

        fun getFromTag(tag: String?): Gender{
            if(tag != null)
                for(gender in values())
                    if(gender.tag == tag.toLowerCase(Locale.getDefault()))
                        return gender
            return NA
        }
    }
}
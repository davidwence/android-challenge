package com.miflink.rickandmorty.domain.usecases

import com.miflink.rickandmorty.domain.repositories.SearchsRepository

class GetRecentSearchs (private val repository: SearchsRepository) {

    suspend fun getAll(): List<String> {
        val list = mutableListOf<String>()
        for(search in repository.getAll().sortedByDescending {
                recentSearch -> recentSearch.timestamp })
            list.add(search.value)
        return list
    }

}
package com.miflink.rickandmorty.domain.entities

import com.google.gson.annotations.SerializedName

data class Place (@SerializedName("name") val name: String? = null,
                  @SerializedName("url") val url: String? = null)
package com.miflink.rickandmorty.domain.entities

import android.text.TextUtils
import com.google.gson.annotations.SerializedName

data class Character (@SerializedName("id") val id: Int? = null,
                      @SerializedName("name") val name: String? = null,
                      @SerializedName("status") val _status: String? = null,
                      @SerializedName("species") val _species: String? = null,
                      @SerializedName("type") private val _type: String? = null,
                      @SerializedName("gender") val _gender: String? = null,
                      @SerializedName("origin") val origin: Place? = null,
                      @SerializedName("location") val location: Place? = null,
                      @SerializedName("image") val image: String? = null,
                      @SerializedName("episode") val episode: List<String>? = null,
                      @SerializedName("url") val url: String? = null,
                      @SerializedName("created") val created: String? = null){

    val status get() = Status.getFromTag(_status)
    val species get() = Species.getFromTag(_species)
    val gender get() = Gender.getFromTag(_gender)

    val formattedSpecies: String get() {
        val tags = mutableListOf(species.label)
        if(_type != null && _type.isNotBlank())
            tags.add(_type)
        return TextUtils.join(" - ", tags)
    }

    val appereances get() = episode?.size ?: 0
}
package com.miflink.rickandmorty.domain.entities

import java.util.*

@Suppress("unused")
enum class Species (val tag: String, val label: String) {
    NA("", "Any"),
    HUMAN("human", "Human"),
    ALIEN("alien", "Alien"),
    HUMANOID("humanoid", "Humanoid"),
    UNKWONW("unknown", "Unknown"),
    PBH("poopybutthole", "Poopybutthole"),
    MYTHOLOG("mytholog", "Mytholog"),
    ANIMAL("animal", "Animal"),
    VAMPIRE("vampire", "Vampire"),
    ROBOT("robot", "Robot"),
    CRONENBERG("cronenberg", "Cronenberg"),
    DISEASE("disease", "Disease"),
    PARASITE("parasite", "Parasite"),;

    override fun toString(): String {
        return label
    }

    companion object{
        const val TITLE = "Species"

        fun getFromTag(tag: String?): Species{
            if(tag != null)
                for(species in values())
                    if(species.tag == tag.toLowerCase(Locale.getDefault()))
                        return species
            return Species.NA
        }
    }
}
package com.miflink.rickandmorty.domain.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class Search(@ColumnInfo(name = "value") val value: String,
                  @ColumnInfo(name = "last_searched") var timestamp: Long = Date().time,
                  @ColumnInfo(name = "hits") var hits: Int = 1){

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0
}
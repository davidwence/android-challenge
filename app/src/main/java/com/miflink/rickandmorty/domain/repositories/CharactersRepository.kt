package com.miflink.rickandmorty.domain.repositories

import com.miflink.rickandmorty.domain.entities.*

interface CharactersRepository {

    suspend fun getCharacters(page: Int, name: String?,
                              status: Status?, species: Species?, gender: Gender?):
            Result<List<Character>>

}
package com.miflink.rickandmorty.domain.entities

sealed class Result<out T> {
    data class Success<out T>(val data: T): Result<T>()
    data class Error(val message: String): Result<Nothing>()
    object Completed: Result<Nothing>()
    object Disconnected: Result<Nothing>()
}
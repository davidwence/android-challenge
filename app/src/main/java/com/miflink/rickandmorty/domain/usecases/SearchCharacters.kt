package com.miflink.rickandmorty.domain.usecases

import com.miflink.rickandmorty.domain.entities.*
import com.miflink.rickandmorty.domain.repositories.CharactersRepository
import java.util.*

class SearchCharacters(private val repository: CharactersRepository) {

    companion object{
        const val SAMESEARCH_ERROR = "The most recent results are shown"
    }

    private var newRequest = true

    private var _page: Int = 1
    private var _name: String? = null
    private var _gender: Gender? = null
    private var _status: Status? = null
    private var _species: Species? = null

    fun setPage(page: Int){
        if(page > 0 && page > _page) {
            newRequest = true
            _page = page
        }
    }

    fun isNewDataSet() = _page == 1

    fun setName(name: String?){
        newRequest = true
        _page = 1
        _name = if(name != null && name.isNotBlank()) name.toLowerCase(Locale.getDefault()) else null
    }

    fun setStatus(status: Status?){
        newRequest = true
        _page = 1
        _status = status
    }

    fun setSpecies(species: Species?){
        newRequest = true
        _page = 1
        _species = species
    }

    fun setGender(gender: Gender?){
        newRequest = true
        _page = 1
        _gender = gender
    }

    fun resetFilters(){
        newRequest = true
        _status = null
        _species = null
        _gender = null
        _page = 1
    }

    suspend operator fun invoke(): Result<List<Character>>{
        return if(newRequest) {
            newRequest = false
            repository.getCharacters(_page, _name, _status, _species, _gender)
        } else
            Result.Error(SAMESEARCH_ERROR)
    }

}
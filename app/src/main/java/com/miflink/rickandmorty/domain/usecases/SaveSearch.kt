package com.miflink.rickandmorty.domain.usecases

import com.miflink.rickandmorty.domain.entities.Search
import com.miflink.rickandmorty.domain.repositories.SearchsRepository
import java.util.*

class SaveSearch (private val repository: SearchsRepository) {

    suspend fun save(search: String): Boolean {
        if(search.isNotBlank()) {
            val previous = repository.getAll()
            for (p in previous)
                if (p.value == search)
                    return repository.update(
                        p.apply {
                            timestamp = Date().time
                            hits = p.hits + 1
                        })
            return repository.save(Search(search.toLowerCase(Locale.getDefault())))
        } else
            return false
    }

}
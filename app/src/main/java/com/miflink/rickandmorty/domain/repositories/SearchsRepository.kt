package com.miflink.rickandmorty.domain.repositories

import com.miflink.rickandmorty.domain.entities.Search

interface SearchsRepository {

    suspend fun save(search: Search): Boolean
    suspend fun update(search: Search): Boolean
    suspend fun getAll(): List<Search>

}
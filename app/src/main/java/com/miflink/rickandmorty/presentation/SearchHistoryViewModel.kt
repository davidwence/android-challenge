package com.miflink.rickandmorty.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.miflink.rickandmorty.domain.usecases.GetRecentSearchs
import com.miflink.rickandmorty.domain.usecases.SaveSearch
import kotlinx.coroutines.launch

class SearchHistoryViewModel(private val saveSearch: SaveSearch,
                             private val getRecentSearchs: GetRecentSearchs) : ViewModel(){

    private val _searchs = MutableLiveData<List<String>>()
    val recentSearchs: LiveData<List<String>> get() = _searchs

    fun save(search: String){
        viewModelScope.launch { saveSearch.save(search) }
    }

    fun loadHistory(){
        viewModelScope.launch { _searchs.value = getRecentSearchs.getAll() }
    }

}
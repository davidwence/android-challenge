package com.miflink.rickandmorty.presentation.ui;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.shehabic.droppy.DroppyClickCallbackInterface;
import com.shehabic.droppy.DroppyMenuItem;
import com.shehabic.droppy.DroppyMenuItemInterface;
import com.shehabic.droppy.DroppyMenuPopup;
import com.shehabic.droppy.DroppyMenuSeparator;
import com.shehabic.droppy.animations.DroppyAnimation;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;

public class ShownCallbackDroppyMenu extends DroppyMenuPopup {

    private boolean isShown = false;

    private ShownCallbackDroppyMenu(Context mContext,
                                    View parentMenuItem,
                                    List<DroppyMenuItemInterface> menuItem,
                                    DroppyClickCallbackInterface droppyClickCallbackInterface,
                                    boolean addTriggerOnAnchorClick,
                                    int popupMenuLayoutResourceId,
                                    OnDismissCallback onDismissCallback) {
        super(mContext, parentMenuItem, menuItem, droppyClickCallbackInterface,
                addTriggerOnAnchorClick, popupMenuLayoutResourceId, onDismissCallback);
    }

    @Override
    public void show() {
        super.show();
        isShown = true;
    }

    @Override
    protected void dismissPopup(boolean itemSelected) {
        try {
            super.dismissPopup(itemSelected);
        } catch (Exception ignored){ }
        isShown = false;
    }

    @Override
    public void dismiss(boolean itemSelected) {
        super.dismiss(itemSelected);
        isShown = false;
    }

    public boolean isShown() {
        return isShown;
    }

    @SuppressWarnings("unused")
    public static class Builder {
        Context ctx;
        View parentMenuItem;
        List<DroppyMenuItemInterface> menuItems = new ArrayList<>();
        DroppyClickCallbackInterface callbackInterface;
        boolean triggerOnAnchorClick = true;
        OnDismissCallback onDismissCallback;
        int offsetX = -20;
        int offsetY = 25;
        DroppyAnimation droppyAnimation;

        public Builder(Context ctx, View parentMenuItem) {
            this.ctx = ctx;
            this.parentMenuItem = parentMenuItem;
        }

        public Builder addMenuItem(DroppyMenuItemInterface droppyMenuItem) {
            menuItems.add(droppyMenuItem);
            return this;
        }

        public Builder addSeparator() {
            menuItems.add(new DroppyMenuSeparator());
            return this;
        }

        public Builder setOnClick(DroppyClickCallbackInterface droppyClickCallbackInterface1) {
            callbackInterface = droppyClickCallbackInterface1;
            return this;
        }

        public Builder setXOffset(int xOffset) {
            this.offsetX = xOffset;
            return this;
        }

        public Builder setYOffset(int yOffset) {
            this.offsetY = yOffset;
            return this;
        }

        public Builder triggerOnAnchorClick(boolean onAnchorClick) {
            triggerOnAnchorClick = onAnchorClick;
            return this;
        }

        public Builder setOnDismissCallback(OnDismissCallback onDismissCallback) {
            this.onDismissCallback = onDismissCallback;
            return this;
        }

        public Builder setPopupAnimation(DroppyAnimation droppyAnimation) {
            this.droppyAnimation = droppyAnimation;
            return this;
        }

        public Builder fromMenu(int menuResourceId) {
            Menu menu = newMenuInstance(ctx);
            MenuInflater menuInflater = new MenuInflater(ctx);
            menuInflater.inflate(menuResourceId, menu);
            int lastGroupId = menu.getItem(0).getGroupId();
            for (int i = 0; i < menu.size(); i++) {
                MenuItem mItem = menu.getItem(i);
                DroppyMenuItem dMenuItem = new DroppyMenuItem(mItem.getTitle().toString());

                if (mItem.getIcon() != null) {
                    dMenuItem.setIcon(mItem.getIcon());
                }

                if (mItem.getItemId() > 0) {
                    dMenuItem.setId(mItem.getItemId());
                }
                if (mItem.getGroupId() != lastGroupId) {
                    menuItems.add(new DroppyMenuSeparator());
                    lastGroupId = mItem.getGroupId();
                }
                menuItems.add(dMenuItem);
            }
            return this;
        }

        Menu newMenuInstance(Context context) {
            try {
                Class<?> menuBuilderClass =
                        Class.forName("com.android.internal.view.menu.MenuBuilder");
                Constructor<?> constructor = menuBuilderClass.getDeclaredConstructor(Context.class);
                return (Menu) constructor.newInstance(context);
            } catch (Exception ignored) { }
            return null;
        }

        public ShownCallbackDroppyMenu build() {
            ShownCallbackDroppyMenu popup =
                    new ShownCallbackDroppyMenu(ctx,
                            parentMenuItem,
                            menuItems,
                            callbackInterface,
                            triggerOnAnchorClick,
                            -1,
                            onDismissCallback);
            popup.setOffsetX(offsetX);
            popup.setOffsetY(offsetY);
            popup.setPopupAnimation(droppyAnimation);
            return popup;
        }
    }
}

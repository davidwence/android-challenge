package com.miflink.rickandmorty.presentation.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.miflink.rickandmorty.R
import com.miflink.rickandmorty.databinding.ItemCharacterBinding
import com.miflink.rickandmorty.domain.entities.Character
import com.miflink.rickandmorty.utils.inflateView
import com.miflink.rickandmorty.utils.layoutInflater

class CharactersAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>(){

    companion object{
        const val ELEMENT = 1
        const val LOADER = 2
    }

    private val items = mutableListOf<Character>()
    private var loaderActive = false

    fun clear(){
        items.clear()
        loaderActive = false
    }

    fun setupInitialData(items: List<Character>){
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun addNewData(items: List<Character>){
        this.items.addAll(items)
        loaderActive = false
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when(viewType){
            LOADER ->
                LoaderViewHolder(parent.inflateView(R.layout.item_loader))
            else ->
                CharacterViewHolder(
                    ItemCharacterBinding.inflate(parent.layoutInflater(), parent,false))
        }
    }

    override fun getItemCount(): Int = items.size + if(loaderActive) 1 else 0

    override fun getItemViewType(position: Int): Int {
        if(position == itemCount - 1 && loaderActive)
            return LOADER
        return ELEMENT
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if(holder.itemViewType == ELEMENT)
            (holder as CharacterViewHolder).bind(items[position])
    }

    fun showLoader(){
        loaderActive = true
        notifyDataSetChanged()
    }

    private inner class LoaderViewHolder(view: View): RecyclerView.ViewHolder(view)

    private inner class CharacterViewHolder(val binding: ItemCharacterBinding):
        RecyclerView.ViewHolder(binding.root) {
        fun bind(element: Character){
            binding.character = element
            binding.executePendingBindings()
        }
    }

}
package com.miflink.rickandmorty.presentation.ui

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.github.zagum.expandicon.ExpandIconView
import com.miflink.rickandmorty.R
import com.miflink.rickandmorty.databinding.ItemFiltermenuExpandBinding

class EnumFilter<E: Enum<*>>(context: Context,
                             parent: ViewGroup,
                             clazz: Class<E>,
                             title: String,
                             private val callback: OnOptionChosenCallback<E>):
    View.OnClickListener {

    interface OnOptionChosenCallback<E: Enum<*>>{
        fun onOptionChosen(option: E)
        fun onOptionRepeated()
    }

    private val binding by lazy {
        ItemFiltermenuExpandBinding.inflate(LayoutInflater.from(context), parent, false) }

    private var defaultElement: E? = null
    private var chosenElement: E? = null
    private var chosenView: View? = null

    init {
        binding.title = title
        clazz.enumConstants?.let {
            for(enum in it){
                if(defaultElement == null)
                    defaultElement = enum
                val option = LayoutInflater.from(context).inflate(
                    R.layout.item_filtermenu_expandoption, binding.options, false) as TextView
                option.text = enum.toString()
                option.tag = enum
                option.setOnClickListener(this)
                if(enum == defaultElement){
                    chosenView = option
                    option.alpha = 1.0f
                }
                binding.options.addView(option)
            }

            chosenElement = defaultElement
            binding.activeOption = defaultElement?.toString()
            binding.selector.setOnClickListener(this)
            parent.addView(binding.root)
        } ?: Log.w(javaClass.simpleName,"No enum constanst found")
    }

    fun restart(){
        chosenElement = defaultElement
        if (chosenView?.tag != defaultElement) {
            chosenView?.alpha = 0.5f
            chosenView = binding.options.getChildAt(0)
            chosenView?.alpha = 1.0f
            binding.activeOption = defaultElement?.toString()
        }
        binding.expandIcon.setState(ExpandIconView.MORE, false)
        binding.options.visibility = View.GONE
    }

    private fun switchFilter(){
        binding.expandIcon.switchState(true)
        binding.options.visibility = if (binding.options.isShown) View.GONE else View.VISIBLE
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.selector -> switchFilter()
            R.id.filterOption -> {
                @Suppress("UNCHECKED_CAST") val element = v.tag as E
                if (element != chosenElement) {
                    v.alpha = 1.0f
                    chosenView?.alpha = 0.5f
                    chosenView = v
                    chosenElement = element
                    switchFilter()
                    binding.activeOption = chosenElement?.toString()
                    callback.onOptionChosen(element)
                } else
                    callback.onOptionRepeated()
            }
        }
    }

}
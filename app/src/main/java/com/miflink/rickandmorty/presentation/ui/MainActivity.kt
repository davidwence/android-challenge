package com.miflink.rickandmorty.presentation.ui

import android.os.Bundle
import android.view.KeyEvent
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doAfterTextChanged
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.RecyclerView
import com.miflink.rickandmorty.R
import com.miflink.rickandmorty.databinding.ActivityMainBinding
import com.miflink.rickandmorty.presentation.SearchCharactersViewModel
import com.miflink.rickandmorty.presentation.SearchHistoryViewModel
import com.miflink.rickandmorty.presentation.adapter.CharactersAdapter
import com.miflink.rickandmorty.presentation.adapter.RecentSearchsAdapter
import com.miflink.rickandmorty.utils.*
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf

//fixme arreglar bug al mostrar sugerencias
//el bug consiste en que, al seleccionar la caja de texto por primera vez e inmediatamente después
//presionar "back" (para ocultar el teclado), el focus no se limpia, eviatando que se muetren las
//sugerencias en adelante
class MainActivity: AppCompatActivity() {

    companion object{
        const val PAGE_SIZE = 20
    }

    private val binding by lazy {
        DataBindingUtil.setContentView<ActivityMainBinding>(this, R.layout.activity_main) }

    //control del filtro del catálogo
    private val searchFilter by lazy {
        SearchFilter(this,
            binding.filter,
            { searchCharactersViewModel.setStatus(it) },
            { searchCharactersViewModel.setSpecies(it) },
            { searchCharactersViewModel.setGender(it) },
            {
                scrollListener.resetState()
                searchCharactersViewModel.clearFilters()
            })
    }

    //control de la lista para realizar paginado
    private val scrollListener by lazy {
        object: EndlessRecyclerViewScrollListener(binding.results.layoutManager, PAGE_SIZE){
            override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView) {
                //si el tamaño de elementos % tamaño de página es 0
                //existe al menos otra página que buscar
                if (totalItemsCount > 0 && totalItemsCount % PAGE_SIZE == 0) {
                    blockUntilMoreLoaded(true)
                    view.post { charactersAdapter.showLoader() }
                    searchCharactersViewModel.setPage(page + 1)
                }
            }
        }
    }

    //búsqueda de personajes
    private val charactersAdapter: CharactersAdapter by inject()
    private val searchCharactersViewModel: SearchCharactersViewModel by viewModel()

    //búsquedas recientes
    private val recentSearchsListener: (String) -> Unit = {
        binding.searchBy.setText(it)
        startSearch()
    }
    private val recentSearchsAdapter: RecentSearchsAdapter by inject {
        parametersOf(recentSearchsListener) }
    private val searchHistoryViewModel: SearchHistoryViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        searchFilter
        //búsqueda por texto
        binding.searchBy.doAfterTextChanged {
            searchCharactersViewModel.setName(it?.toString())
        }
        binding.searchBy.setOnEditorActionListener { _, keyCode, event ->
            if ((event != null && event.action == KeyEvent.ACTION_UP &&
                        event.keyCode == KeyEvent.KEYCODE_ENTER) ||
                keyCode == EditorInfo.IME_ACTION_SEARCH)
                startSearch()
            return@setOnEditorActionListener false
        }
        binding.searchBy.setOnFocusChangeListener { _, hasFocus ->
            if(hasFocus)
                showRecentSearchs()
        }

        //cambios en UI al cargar la lista
        searchCharactersViewModel.searchStep.observe(this, Observer { status ->
            when(status){
                is SearchCharactersViewModel.Step.InitialLoading -> {
                    //con cada nueva carga desactivo los controles para evitar nuevas peticiones
                    //hasta terminar la primer petición
                    binding.filter.disable()
                    binding.searchBy.disable()
                    binding.loader.show()
                    binding.results.hide()
                    charactersAdapter.clear()
                }
                is SearchCharactersViewModel.Step.InitialDataLoaded -> {
                    //se reactivan los controles y se muestra el resultado
                    binding.filter.enable()
                    binding.searchBy.enable()
                    binding.loader.hide()
                    binding.results.show()
                }
                is SearchCharactersViewModel.Step.InitialError -> {
                    //se reactivan los controles y se muestra el error
                    binding.filter.enable()
                    binding.searchBy.enable()
                    binding.loader.hide()
                    showSnackbar(status.message)
                }
            }
        })

        //cuando se recibe una nueva lista se procesa aquí
        searchCharactersViewModel.initialData.observe(this, Observer { data ->
            charactersAdapter.setupInitialData(data)
            searchHistoryViewModel.save(binding.searchBy.extractData())
        })
        //las páginas adicionales se procesa aquí
        searchCharactersViewModel.additionalData.observe(this, Observer { data ->
            scrollListener.blockUntilMoreLoaded(false)
            charactersAdapter.addNewData(data)
        })

        //al solicitar las búsquedas mas recientes se renderizan
        searchHistoryViewModel.recentSearchs.observe(this, Observer { data ->
            if(data.isNotEmpty())
                binding.noRecentSearchs.hide()
            recentSearchsAdapter.setupInitialData(data)
        })

        binding.results.apply {
            setHasFixedSize(true)
            adapter = charactersAdapter
            addOnScrollListener(scrollListener)
        }

        binding.recentSearchs.apply {
            setHasFixedSize(true)
            adapter = recentSearchsAdapter
        }
    }

    override fun onStart() {
        super.onStart()
        startSearch()
    }

    override fun onBackPressed() {
        if(!searchFilter.hideFilter())
            if(!hideRecentSearchs())
                super.onBackPressed()
    }

    private fun startSearch(){
        //busqueda inicial con todos los personajes sin filtros
        hideRecentSearchs()
        binding.searchBy.clearFocus()
        hideKeyboard()
        searchCharactersViewModel.search()
    }

    private fun showRecentSearchs(){
        binding.recentSearchs.show()
        binding.noRecentSearchs.show()
        searchHistoryViewModel.loadHistory()
    }

    private fun hideRecentSearchs(): Boolean{
        if(binding.recentSearchs.isShown){
            binding.recentSearchs.hide()
            binding.noRecentSearchs.hide()
            return true
        }
        return false
    }

}
package com.miflink.rickandmorty.presentation.adapter

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.miflink.rickandmorty.databinding.ItemRecentsearchBinding
import com.miflink.rickandmorty.utils.layoutInflater

class RecentSearchsAdapter(private val onOptionChosen: (option: String) -> Unit):
    RecyclerView.Adapter<RecentSearchsAdapter.RecentSearchViewHolder>(),
    View.OnClickListener {

    private val items = mutableListOf<String>()

    fun setupInitialData(items: List<String>){
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecentSearchViewHolder {
        return RecentSearchViewHolder(
            ItemRecentsearchBinding.inflate(parent.layoutInflater(), parent, false), this)
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: RecentSearchViewHolder, position: Int) {
        holder.onBind(items[position])
    }

    override fun onClick(v: View?) {
        if(v != null)
            onOptionChosen(v.tag as String)
    }

    inner class RecentSearchViewHolder(private val binding: ItemRecentsearchBinding,
                                       clickListener: View.OnClickListener):
        RecyclerView.ViewHolder(binding.root){

        init {
            binding.root.setOnClickListener(clickListener)
        }

        fun onBind(search: String){
            binding.search = search
            binding.executePendingBindings()
        }

    }
}
package com.miflink.rickandmorty.presentation

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.miflink.rickandmorty.domain.entities.*
import com.miflink.rickandmorty.domain.usecases.SearchCharacters
import kotlinx.coroutines.launch

class SearchCharactersViewModel (private val searchCharacters: SearchCharacters): ViewModel(){

    sealed class Step {
        object InitialLoading: Step()
        object InitialDataLoaded: Step()
        object PageLoading: Step()
        object PageLoaded: Step()
        class InitialError(val message: String = ""): Step()
    }

    private val _initialData = MutableLiveData<List<Character>>()
    val initialData: LiveData<List<Character>> get() = _initialData
    private val _additionData = MutableLiveData<List<Character>>()
    val additionalData: LiveData<List<Character>> get() = _additionData

    private val _status = MutableLiveData<Step>()
    val searchStep: LiveData<Step> get() = _status

    fun setName(name: String?){
        searchCharacters.setName(name)
    }
    fun setStatus(status: Status?){
        searchCharacters.setStatus(status)
        search()
    }
    fun setSpecies(species: Species?){
        searchCharacters.setSpecies(species)
        search()
    }
    fun setGender(gender: Gender?){
        searchCharacters.setGender(gender)
        search()
    }
    fun setPage(page: Int){
        searchCharacters.setPage(page)
        search()
    }
    fun clearFilters(){
        searchCharacters.resetFilters()
        search()
    }

    fun search(){
        _status.value = if(searchCharacters.isNewDataSet()) Step.InitialLoading else Step.PageLoading
        viewModelScope.launch {
            when(val result = searchCharacters()){
                is Result.Success -> {
                    if(searchCharacters.isNewDataSet()) {
                        _status.value = Step.InitialDataLoaded
                        _initialData.value = result.data
                    } else {
                        _status.value = Step.PageLoaded
                        _additionData.value = result.data
                    }
                }
                is Result.Error ->
                    if(searchCharacters.isNewDataSet())
                        _status.value = Step.InitialError(result.message)

                is Result.Disconnected ->
                    if(searchCharacters.isNewDataSet())
                        _status.value = Step.InitialError("Check your internet connection")
            }
        }

    }

}
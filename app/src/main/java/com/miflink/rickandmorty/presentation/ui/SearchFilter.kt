package com.miflink.rickandmorty.presentation.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import com.miflink.rickandmorty.databinding.ItemFiltermenuBinding
import com.miflink.rickandmorty.domain.entities.Gender
import com.miflink.rickandmorty.domain.entities.Species
import com.miflink.rickandmorty.domain.entities.Status
import com.shehabic.droppy.DroppyMenuCustomItem
import com.shehabic.droppy.animations.DroppyFadeInAnimation

class SearchFilter(context: Context,
                   callingView: View,
                   private val statusChanged: (status: Status?) -> Unit,
                   private val speciesChanged: (species: Species?) -> Unit,
                   private val genderChanged: (gender: Gender?) -> Unit,
                   private val filtersCleared: () -> Unit) {

    private val binding by lazy {
        ItemFiltermenuBinding.inflate(LayoutInflater.from(context),null, false) }

    private val droppyMenu by lazy {
        ShownCallbackDroppyMenu.Builder(context, callingView)
            .addMenuItem(DroppyMenuCustomItem(binding.root))
            .setPopupAnimation(DroppyFadeInAnimation())
            .build() }

    private val statusFilter by lazy {
        EnumFilter(context, binding.filterContents, Status::class.java, Status.TITLE,
        object: EnumFilter.OnOptionChosenCallback<Status> {
            override fun onOptionChosen(option: Status) {
                if(option != Status.NA)
                    statusChanged(option)
                else
                    statusChanged(null)
            }

            override fun onOptionRepeated() {
                droppyMenu.dismiss(false)
            }
        }) }

    private val speciesFilter by lazy {
        EnumFilter(context, binding.filterContents, Species::class.java, Species.TITLE,
            object: EnumFilter.OnOptionChosenCallback<Species> {
                override fun onOptionChosen(option: Species) {
                    if(option != Species.NA)
                        speciesChanged(option)
                    else
                        speciesChanged(null)
                }

                override fun onOptionRepeated() {
                    droppyMenu.dismiss(false)
                }
            }) }

    private val genderFilter by lazy {
        EnumFilter(context, binding.filterContents, Gender::class.java, Gender.TITLE,
            object: EnumFilter.OnOptionChosenCallback<Gender> {
                override fun onOptionChosen(option: Gender) {
                    if(option != Gender.NA)
                        genderChanged(option)
                    else
                        genderChanged(null)
                }

                override fun onOptionRepeated() {
                    droppyMenu.dismiss(false)
                }
            }) }

    init {
        binding.clear.setOnClickListener {
            statusFilter.restart()
            speciesFilter.restart()
            genderFilter.restart()
            filtersCleared()
            droppyMenu.dismiss(false)
        }
        droppyMenu
        statusFilter
        speciesFilter
        genderFilter
    }

    fun hideFilter(): Boolean{
        if(droppyMenu.isShown){
            droppyMenu.dismiss(false)
            return true
        }
        return false
    }

}
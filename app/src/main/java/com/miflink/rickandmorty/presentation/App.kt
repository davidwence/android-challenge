package com.miflink.rickandmorty.presentation

import android.app.Application
import com.miflink.rickandmorty.data.mainModule
import com.miflink.rickandmorty.data.searchHistoryModule
import com.miflink.rickandmorty.data.searchModule
import com.miflink.rickandmorty.data.servicesModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App: Application() {

    override fun onCreate() {
        super.onCreate()
        // Start Koin
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(listOf(mainModule, searchModule, servicesModule, searchHistoryModule))
        }
    }

}
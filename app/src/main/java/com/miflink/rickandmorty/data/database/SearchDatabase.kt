package com.miflink.rickandmorty.data.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.miflink.rickandmorty.domain.entities.Search

@Database(entities = [Search::class], version = 1)
abstract class SearchDatabase: RoomDatabase() {

    abstract fun searchDao(): SearchDao

    companion object{
        private const val DATABASE_NAME = "searchs_database"
        @Volatile
        private var INSTANCE: SearchDatabase? = null

        fun getInstance(context: Context): SearchDatabase? {
            INSTANCE ?: synchronized(this) {
                INSTANCE = Room.databaseBuilder(
                    context.applicationContext,
                    SearchDatabase::class.java,
                    DATABASE_NAME
                ).build()
            }
            return INSTANCE
        }

        fun destroy(){
            INSTANCE = null
        }
    }

}
package com.miflink.rickandmorty.data.database

import android.util.Log
import com.miflink.rickandmorty.domain.entities.Search
import com.miflink.rickandmorty.domain.repositories.SearchsRepository

class RoomSearchsRepository(private val searchDao: SearchDao?): SearchsRepository {

    override suspend fun save(search: Search): Boolean {
        searchDao?.let {
            it.insert(search)
            Log.d("RoomSearchsRepository","Term '${search.value}' saved")
            return true
        } ?: return false
    }

    override suspend fun update(search: Search): Boolean {
        searchDao?.let {
            it.update(search)
            Log.d("RoomSearchsRepository","Term '${search.value}' updated, hits: ${search.hits}")
            return true
        } ?: return false
    }

    override suspend fun getAll(): List<Search> = searchDao?.getAll() ?: listOf()

}
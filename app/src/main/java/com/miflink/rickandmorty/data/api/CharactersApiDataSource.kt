package com.miflink.rickandmorty.data.api

import android.util.Log
import com.google.gson.Gson
import com.miflink.rickandmorty.domain.entities.*
import com.miflink.rickandmorty.utils.ws.ErrorResponse
import com.miflink.rickandmorty.utils.ws.handleThrowable

class CharactersApiDataSource(private val apiService: ApiService){

    suspend fun getCharacters(page: Int, name: String?,
                              status: Status?, species: Species?, gender: Gender?):
            Result<List<Character>> {
        try {
            val response = apiService.getCharacters(page, name, status?.tag, species?.tag, gender?.tag)
            if(response.isSuccessful) {
                return response.body()?.let { body ->
                    if (body.info != null && body.results != null)
                        Result.Success(body.results)
                    else
                        Result.Error("Data not found")
                } ?: Result.Error("Data not found")
            } else {
                try {
                    @Suppress("BlockingMethodInNonBlockingContext")
                    response.errorBody()?.string()?.let {
                        Log.d("APISource","response: $it")
                        val errorResponse = Gson().fromJson(it, ErrorResponse::class.java)
                        if(errorResponse?.error != null)
                            return Result.Error(errorResponse.error)
                    }
                } catch (ex: Exception){
                    ex.printStackTrace()
                }
                return Result.Error("Unknown error")
            }
        } catch (t: Throwable){
            t.printStackTrace()
            return Result.Error(handleThrowable(t))
        }
    }
}
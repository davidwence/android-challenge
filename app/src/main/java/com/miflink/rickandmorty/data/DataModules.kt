package com.miflink.rickandmorty.data

import android.content.Context
import com.miflink.rickandmorty.data.api.ApiCharactersRepository
import com.miflink.rickandmorty.data.api.ApiService
import com.miflink.rickandmorty.data.api.CharactersApiDataSource
import com.miflink.rickandmorty.data.database.RoomSearchsRepository
import com.miflink.rickandmorty.data.database.SearchDao
import com.miflink.rickandmorty.data.database.SearchDatabase
import com.miflink.rickandmorty.domain.repositories.CharactersRepository
import com.miflink.rickandmorty.domain.repositories.SearchsRepository
import com.miflink.rickandmorty.domain.usecases.GetRecentSearchs
import com.miflink.rickandmorty.domain.usecases.SaveSearch
import com.miflink.rickandmorty.domain.usecases.SearchCharacters
import com.miflink.rickandmorty.presentation.SearchCharactersViewModel
import com.miflink.rickandmorty.presentation.SearchHistoryViewModel
import com.miflink.rickandmorty.presentation.adapter.CharactersAdapter
import com.miflink.rickandmorty.presentation.adapter.RecentSearchsAdapter
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

private const val BASE_URL = "https://rickandmortyapi.com/"

val mainModule = module {
    //buscador
    factory { CharactersAdapter() }
    viewModel { SearchCharactersViewModel(searchCharacters = get()) }
    //historial de búsqueda
    factory { (onOptionChosen: (String) -> Unit) -> RecentSearchsAdapter(onOptionChosen) }
    viewModel { SearchHistoryViewModel(saveSearch = get(), getRecentSearchs = get()) }
}

val searchModule = module {
    single { SearchCharacters(repository = get()) }
    single<CharactersRepository> { ApiCharactersRepository(apiDataSource = get()) }
    factory { CharactersApiDataSource(apiService = get()) }
    factory { provideApiService(retrofit = get()) }
}

val searchHistoryModule = module {
    single { SaveSearch(repository = get()) }
    single { GetRecentSearchs(repository = get()) }
    single<SearchsRepository> { RoomSearchsRepository(searchDao = get()) }
    factory { provideSearchDao(context = get()) }
}

val servicesModule = module {
    single { provideRetrofitInstance() }
}

private fun provideSearchDao(context: Context): SearchDao? =
    SearchDatabase.getInstance(context)?.searchDao()

private fun provideApiService(retrofit: Retrofit): ApiService =
    retrofit.create(ApiService::class.java)

private fun provideRetrofitInstance(): Retrofit = Retrofit.Builder()
    .client(
        OkHttpClient.Builder()
        .readTimeout(60, TimeUnit.SECONDS)
        .connectTimeout(60, TimeUnit.SECONDS)
        .addInterceptor(
            run {
                val httpLoggingInterceptor = HttpLoggingInterceptor()
                httpLoggingInterceptor.apply {
                    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
                }
            }
        ).build())
    .baseUrl(BASE_URL)
    .addConverterFactory(GsonConverterFactory.create())
    .build()
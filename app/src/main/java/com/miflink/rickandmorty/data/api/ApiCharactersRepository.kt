package com.miflink.rickandmorty.data.api

import com.miflink.rickandmorty.domain.entities.*
import com.miflink.rickandmorty.domain.repositories.CharactersRepository

class ApiCharactersRepository(private val apiDataSource: CharactersApiDataSource): CharactersRepository {

    override suspend fun getCharacters(page: Int, name: String?,
                                       status: Status?, species: Species?, gender: Gender?):
            Result<List<Character>> {
        return apiDataSource.getCharacters(page, name, status, species, gender)
    }
}
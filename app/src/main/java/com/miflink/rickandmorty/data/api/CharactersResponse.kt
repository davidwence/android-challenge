package com.miflink.rickandmorty.data.api

import com.google.gson.annotations.SerializedName
import com.miflink.rickandmorty.domain.entities.Character

class CharactersResponse (@SerializedName("info") val info: Info? = null,
                          @SerializedName("results") val results: List<Character>? = null)
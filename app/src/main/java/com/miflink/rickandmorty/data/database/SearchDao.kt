package com.miflink.rickandmorty.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.miflink.rickandmorty.domain.entities.Search

@Dao
interface SearchDao {

    @Query("SELECT * FROM Search")
    suspend fun getAll(): List<Search>

    @Insert
    suspend fun insert(search: Search)

    @Update
    suspend fun update(search: Search)

}
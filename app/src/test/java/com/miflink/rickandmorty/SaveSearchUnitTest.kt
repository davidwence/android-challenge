package com.miflink.rickandmorty

import com.miflink.rickandmorty.data.testSearchHistoryModule
import com.miflink.rickandmorty.domain.repositories.SearchsRepository
import com.miflink.rickandmorty.domain.usecases.SaveSearch
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertFalse
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.koin.core.parameter.parametersOf
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.inject

class SaveSearchUnitTest: KoinTest{

    private val repository by inject<SearchsRepository>()
    private val useCase by inject<SaveSearch>{ parametersOf(repository) }

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        printLogger()
        modules(testSearchHistoryModule)
    }

    @Test
    fun new_search_is_saved(){
        //probar que una búsqueda nueva será guardada
        val success = runBlocking { useCase.save("beth") }
        assertTrue(success)
    }

    @Test
    fun empty_search_is_rejected(){
        //probar que cadenas no válidas no serán guardadas
        val success = runBlocking { useCase.save("") }
        assertFalse(success)
    }

    @Test
    fun repeated_search_is_updated(){
        //probar que una búsqueda repetida también será salvada
        val firstHit = runBlocking { useCase.save("jerry") }
        val secondHit = runBlocking { useCase.save("jerry") }
        assertTrue(firstHit == secondHit)
    }

}
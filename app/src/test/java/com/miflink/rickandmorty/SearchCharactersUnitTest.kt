package com.miflink.rickandmorty

import com.miflink.rickandmorty.data.testSearchModule
import com.miflink.rickandmorty.domain.entities.*
import com.miflink.rickandmorty.domain.usecases.SearchCharacters
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.CoreMatchers.not
import org.junit.Assert.assertThat
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.inject
import java.util.*

class SearchCharactersUnitTest: KoinTest {

    private val useCase by inject<SearchCharacters>()

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        printLogger()
        modules(testSearchModule)
    }

    @Test
    fun search_by_name(){
        val search = "rick"
        useCase.setName(search)
        val result = runBlocking { (useCase() as Result.Success).data }
        for(ricks in result)
            assertTrue(ricks.name?.toLowerCase(Locale.getDefault())?.contains(search) == true)
    }

    @Test
    fun filter_by_status(){
        val status = Status.ALIVE
        useCase.setStatus(status)
        val result = runBlocking { (useCase() as Result.Success).data }
        for(alives in result)
            assertTrue(alives.status == status)
    }

    @Test
    fun filter_by_species(){
        val species = Species.HUMAN
        useCase.setSpecies(species)
        val result = runBlocking { (useCase() as Result.Success).data }
        for(humans in result)
            assertTrue(humans.species == species)
    }

    @Test
    fun filter_by_gender(){
        val gender = Gender.FEMALE
        useCase.setGender(gender)
        val result = runBlocking { (useCase() as Result.Success).data }
        for(females in result)
            assertTrue(females.gender == gender)
    }

    @Test
    fun filters_are_resetting(){
        //probar que el reiniciar filtros y volver a buscar reinicia efectivamente los resultados de búsqueda
        useCase.setGender(Gender.MALE)
        useCase.setSpecies(Species.HUMAN)
        useCase.setStatus(Status.ALIVE)
        val filterResult = runBlocking { (useCase() as Result.Success).data }

        useCase.resetFilters()
        val notFilteredResult = runBlocking { (useCase() as Result.Success).data }
        assertThat(filterResult, `is`(not(notFilteredResult)))
    }

    @Test
    fun non_valid_page_number_is_ignored(){
        //probar que intentar consultar la página 0 no es posible,
        //el caso de uso toma cualquier número no válido y lo devuelve a 1
        //cuando se considera que el dataset será "nuevo"
        useCase.setPage(0)
        assertTrue(useCase.isNewDataSet())
    }

    @Test
    fun empty_text_search_is_ignored(){
        //probar que al buscar con cadenas vacías da el mismo resultado que buscar con nulos
        //el repositorio debe devolver la página inicial
        val emptyStringResult = runBlocking {
            (useCase.apply { setName(" ") }() as Result.Success).data }
        val nullStringResult = runBlocking {
            (useCase.apply { setName(null) }() as Result.Success).data }
        assertThat(emptyStringResult, `is`(nullStringResult))
    }

    @Test
    fun repeated_search_is_rejected(){
        //probar que repetir dos veces la misma consulta (si la primera fue exitosa) debe devolver un error
        //la primer búsqueda debe ser exitosa
        useCase.setName("rick")
        val first: Result<List<Character>> = runBlocking { useCase() }
        assertTrue(first is Result.Success)

        //se vuelve a ejecutar la misma búsqueda sin cambiar parámetros
        val second: Result<List<Character>> = runBlocking { useCase() }
        assertTrue(second is Result.Error && second.message == SearchCharacters.SAMESEARCH_ERROR)
    }

}
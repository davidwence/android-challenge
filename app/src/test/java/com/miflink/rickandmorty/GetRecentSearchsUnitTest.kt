package com.miflink.rickandmorty

import com.miflink.rickandmorty.data.testSearchHistoryModule
import com.miflink.rickandmorty.domain.repositories.SearchsRepository
import com.miflink.rickandmorty.domain.usecases.GetRecentSearchs
import com.miflink.rickandmorty.domain.usecases.SaveSearch
import kotlinx.coroutines.delay
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertTrue
import org.junit.Rule
import org.junit.Test
import org.koin.core.parameter.parametersOf
import org.koin.test.KoinTest
import org.koin.test.KoinTestRule
import org.koin.test.inject

class GetRecentSearchsUnitTest: KoinTest {

    private val repository by inject<SearchsRepository>()
    private val saveUseCase by inject<SaveSearch> { parametersOf(repository) }
    private val useCase by inject<GetRecentSearchs> { parametersOf(repository) }

    @get:Rule
    val koinTestRule = KoinTestRule.create {
        printLogger()
        modules(testSearchHistoryModule)
    }

    @Test
    fun recent_search_as_first_element(){
        val value = "meeseks"
        val first = runBlocking {
            saveUseCase.save(value)
            useCase.getAll()[0]
        }
        assertTrue(first == value)
    }

    @Test
    fun repeated_search_is_updated(){
        val jessica = "jessica"
        val firstSave = runBlocking {
            saveUseCase.save(jessica)
            useCase.getAll()[0]
        }
        val secondSave =runBlocking {
            saveUseCase.save("beth")
            delay(300)
            saveUseCase.save("jerry")
            delay(300)
            saveUseCase.save("summer")
            useCase.getAll()[0]
        }
        val finalSave = runBlocking {
            delay(300)
            saveUseCase.save(jessica)
            useCase.getAll()[0]
        }
        assertTrue(firstSave != secondSave && firstSave == finalSave)
    }

}
package com.miflink.rickandmorty.data

import com.miflink.rickandmorty.domain.repositories.CharactersRepository
import com.miflink.rickandmorty.domain.repositories.SearchsRepository
import com.miflink.rickandmorty.domain.usecases.GetRecentSearchs
import com.miflink.rickandmorty.domain.usecases.SaveSearch
import com.miflink.rickandmorty.domain.usecases.SearchCharacters
import org.koin.dsl.module

val testSearchModule = module {
    single { SearchCharacters(repository = get()) }
    single<CharactersRepository> { TestCharactersRepository(dataSource = get()) }
    factory { TestCharactersDataSource() }
}

val testSearchHistoryModule = module {
    single { (repository: SearchsRepository) -> SaveSearch(repository) }
    single { (repository: SearchsRepository) -> GetRecentSearchs(repository) }
    single<SearchsRepository> { TestSearchsRepository(dataSource = get()) }
    single { TestSearchsDataSource() }
}

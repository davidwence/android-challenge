package com.miflink.rickandmorty.data

import com.miflink.rickandmorty.domain.entities.Search

class TestSearchsDataSource {

    private val searchs = mutableListOf(
        Search("morty", 1595326025245),
        Search("summer", 1595326011203),
        Search("rick",1595326035120))

    fun insert(search: Search): Boolean{
        var found = false
        for(s in searchs)
            if(s.value == search.value){
                found = true
                break
            }
        return if(!found) {
            searchs.add(search)
            true
        } else {
            println("Search '${search.value}' already found, cannot be inserted")
            false
        }
    }

    fun update(search: Search): Boolean{
        var index = -1
        for(i in 0 until searchs.size) {
            val s = searchs[i]
            if (s.value == search.value){
                index = i
                break
            }
        }
        return if(index > -1) {
            searchs[index] = search
            true
        } else {
            println("Search '${search.value}' not found, cannot be updated")
            false
        }
    }

    fun getAll() = searchs

}
package com.miflink.rickandmorty.data

import com.google.gson.annotations.SerializedName
import com.miflink.rickandmorty.domain.entities.Character

data class JSONDataSource(@SerializedName("data") val list: List<Character>)
package com.miflink.rickandmorty.data

import com.google.gson.Gson
import com.miflink.rickandmorty.domain.entities.Character
import com.miflink.rickandmorty.domain.entities.Gender
import com.miflink.rickandmorty.domain.entities.Species
import com.miflink.rickandmorty.domain.entities.Status
import java.util.*

class TestCharactersDataSource {

    companion object{
        private const val PAGESIZE = 5
    }

    private val characters = mutableListOf<Character>()

    init {
        javaClass.classLoader?.let { classLoader ->
            val txt: String = classLoader.getResourceAsStream("testDataSource.txt")
                .bufferedReader().use { it.readText() }
            val dataSource = Gson().fromJson(txt, JSONDataSource::class.java)
            characters.addAll(dataSource.list)
        } ?: println("Cannot load test repository")
    }

    fun getCharactersByFilter(page: Int, name: String?,
                              status: Status?, species: Species?, gender: Gender?):
            List<Character>{
        val results = mutableListOf<Character>()
        val startIndex = (page-1) * PAGESIZE
        if(startIndex < characters.size) {
            for (i in startIndex until startIndex + PAGESIZE) {
                val character = characters[i]
                val withName = name == null ||
                        character.name?.toLowerCase(Locale.getDefault())?.contains(name) == true
                val witStatus = status == null || character.status == status
                val withSpecies = species == null || character.species == species
                val withGender = gender == null || character.gender == gender
                //si todos los filtros se cumplen, se agrega al resultado
                if (withName && witStatus && withSpecies && withGender)
                    results.add(character)
            }
        } else
            println("Page not found: $page")
        return results
    }


}
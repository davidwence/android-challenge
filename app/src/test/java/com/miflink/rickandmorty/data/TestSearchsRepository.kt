package com.miflink.rickandmorty.data

import com.miflink.rickandmorty.domain.entities.Search
import com.miflink.rickandmorty.domain.repositories.SearchsRepository

class TestSearchsRepository(private val dataSource: TestSearchsDataSource): SearchsRepository {

    override suspend fun save(search: Search): Boolean = dataSource.insert(search)
    override suspend fun update(search: Search): Boolean = dataSource.update(search)
    override suspend fun getAll(): List<Search> = dataSource.getAll()

}
package com.miflink.rickandmorty.data

import com.miflink.rickandmorty.domain.entities.*
import com.miflink.rickandmorty.domain.repositories.CharactersRepository

class TestCharactersRepository(private val dataSource: TestCharactersDataSource): CharactersRepository {

    override suspend fun getCharacters(page: Int, name: String?,
                                       status: Status?, species: Species?, gender: Gender?):
            Result<List<Character>> {
        val results =
            dataSource.getCharactersByFilter(page, name, status, species, gender)
        if(results.isNotEmpty())
            return Result.Success(results)
        return Result.Error("There is nothing in here")
    }

}